"use strict";

let userNum = prompt("Enter Fibonacci number:");

while (userNum === "" || userNum === null || isNaN(+userNum)) {
  userNum = prompt("Enter correct Fibonacci number!:", userNum);
}

let F0 = 0;
let F1 = 1;
// let res = 0;

// function fibonacciNums(f0, f1, n) {
//   let res = 0;
//   for (let i = 2; i <= n; i++) {
//     res = f0 + f1;
//     f0 = f1;
//     f1 = res;
//     console.log(f0, f1, res);
//   }
//   return res;
// }

function fibonacciNums2(f0, f1, n) {
  let k = 1;
  if (n < 0) {
    k = -1;
  }
  if (n === 0) {
    return f0;
  } else if (n === 1) {
    return f1;
  }
  let res = f0 + f1 * k;
  return fibonacciNums2(f1, res, n - k);
}

// alert(fibonacciNums(F0, F1, +userNum));

console.log(fibonacciNums2(F0, F1, +userNum));
